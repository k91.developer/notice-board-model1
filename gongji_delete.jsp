<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<%@ page import="java.util.*, java.text.*"  %>
<html>
<head>
<title>KSW01_Board</title>
<%	// 한글 깨짐 방지
	request.setCharacterEncoding("UTF-8");
	// 해당 id를 조회하기 위한 파라미터
	int id;
	String ids = request.getParameter("id");
	id=Integer.parseInt(ids);
	int rootid;
	String rootids = request.getParameter("rootid");
	rootid=Integer.parseInt(rootids);
	String recnt = request.getParameter("recnt");
	String path = request.getParameter("file");
%>
</head>
<body>
<%	// 객체 생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB연결
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	// 해당 id의 데이터를 삭제하기 위한 sql문	
	Statement stmt = conn.createStatement();
	// 삭제 후 recnt을 땡기기 위한 sql문
	Statement stmt2 = conn.createStatement();
	//out.println("rootid:"+rootid);
	//out.println("recnt:"+recnt);
	try{
		stmt.execute("delete from gongji2 where id="+id+";");
		stmt2.execute("update gongji2 set recnt=recnt-1 where rootid="+rootid+" and recnt >= "+recnt+";");
		//닫기		
		stmt2.close();
		stmt.close();
		conn.close();
		
		///// 파일을 제거하기 위한 과정
		String opath="/var/lib/tomcat8/webapps/ROOT/";
		String rpath=opath+path;
		File file = new File(rpath);
		if( file.exists() ){
		// 파일이 존재하는 확인     
			if(file.delete()){
				 //out.println("파일삭제 성공");
			}else{
				//out.println("파일삭제 실패");
			}
		}else{
		// 파일이 존재하지 않을 경우
			//out.println("파일이 존재하지 않습니다.");
			//out.println(rpath);
		}
		
		%> <!-- 삭제 불가할 경우 알림 (다른 누군가가 삭제했을 경우 발생)-->
		<script type="text/javascript">
		alert("데이터가 삭제되었습니다.");
		window.location.href="gongji_list.jsp";
		</script>
	<%
	}catch(Exception e){
		%> <!-- 삭제 불가할 경우 알림 (다른 누군가가 삭제했을 경우 발생)-->
		<script type="text/javascript">
		alert("없는 데이터 입니다.");
		window.location.href="gongji_list.jsp";
		</script>
	<%
	}
	%>
</body>
</html>