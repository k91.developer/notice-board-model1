<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*,javax.sql.*,java.io.*,java.util.*,java.text.*" %>
<html>
<head>
<title>KSW01_Board</title>

<!-- include libraries(jQuery, bootstrap) -->
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 
<!-- include summernote css/js-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>

<%	// 현재 시간을 가져오기 위한
	TimeZone tz;
	java.util.Date date = new java.util.Date();
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	tz = TimeZone.getTimeZone("Asia/Seoul"); df.setTimeZone(tz);
%>
<!--mystyle.css를 적용하기 위해 import-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<script>
<!--특수문자 입력을 막기 위한 함수-->
function checkNumber()
{
	var objEv = event.srcElement;
	var num ="{}[]()<>?_|~`!@#$%^&*-+\"'\\/";    //입력을 막을 특수문자 기재.
	event.returnValue = true;
  
	for (var i=0;i<objEv.value.length;i++){
		if(-1 != num.indexOf(objEv.value.charAt(i)))
		event.returnValue = false;
	}
	if (!event.returnValue){
		alert("특수문자는 입력하실 수 없습니다.");
		var a="";
		for (var i=0;i<objEv.value.length-1;i++){
			a+=objEv.value.charAt(i);
		}
		objEv.value=a;
	}
}
</script>

<!-- summernote에 대한 sytle-->
<script type="text/javascript">
    $(function() {
      $('#summernote').summernote({
        placeholder: 'Hello',
        tabsize: 2,
		height: 500,
		
      });
      
    });
  </script>

</head>
<body>
<center>
<table id="top" cellspacing="0">
		<tr>
			<td><h1 style="font-weight: 700;">게시글 작성</h1></td>
		</tr>
</table>
	<form method="post" action="gongji_write.jsp" enctype="multipart/form-data">
	<table border="1" cellspacing="0">
		<tr>
			<th width="10%">번호</th>
			<td width="90%"><input type="hidden" name="id" value="INSERT">신규(INSERT)</td>
		</tr>
		<tr>
			<th>제목</th>
			<td><input type="text" name="title" onKeyDown="checkNumber();" pattern="^[\S]+" required maxlength="70" placeholder="70자 이내로 작성하시오.(특수문자 제외)"></td>
		</tr>
		<tr>
			<th>일자</th>
			<td><%=df.format(date)%></td>
		</tr>
		<!-- get하는 곳이 content이므로 무엇을 테스트하느냐에 따라 name을 content로 설정할 것!!!-->
		<!--
		<tr>
			<th>내용</th>
			<td><textarea name="content2" onKeyDown="checkNumber();" required maxlength="1000"></textarea></td>	
		</tr>
		-->
		<tr>
			<th>내용</th>
			<td><textarea id="summernote" name="content"></textarea></td>	
		</tr>
		<!--/////////////////////////////////////////////////////////////////// -->
		<tr>
			<th>첨부</th>
			<td><input type="file" name="file"></td>
		</tr>
	</table>
	<table cellspacing="0">
		<tr>
			<td><input type="submit" value="쓰기">
	</form>	
				<input type="button" class="button" value="취소" onclick="location.href='gongji_list.jsp'">
			</td>
		</tr>
	</table>
</center>
</body>
</html>