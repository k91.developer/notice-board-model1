<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*,javax.sql.*,java.io.*,java.util.*,java.text.*" %>
<!DOCTYPE html>
<html>
<head>
<title>KSW01_Board</title>
<!--전역변수 sql 선언-->
<%!String sql;%>
<%! int block; %>

<%	// 한국시간 기준으로 오늘 날짜를 가져오기
	TimeZone tz;
	java.util.Date date = new java.util.Date();
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	tz = TimeZone.getTimeZone("Asia/Seoul"); df.setTimeZone(tz);
	String today = df.format(date);
%>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">

</head>
<body>
<center>
<%	// 한글 깨짐 방지
	request.setCharacterEncoding("UTF-8");
	//검색기능을 위한 파라미터
	String search = request.getParameter("search");
	String opt = request.getParameter("opt");
	// 객체 생성
	Class.forName("com.mysql.jdbc.Driver");
	// mysql 데이터베이스 연결
	try{
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
		// stmt 게시물이 있는지 판단 위한
		Statement stmt = conn.createStatement();
		// stmt2 게시판 출력
		Statement stmt2 = conn.createStatement();
						
		// 게시물 존재 여부를 확인하기 위한 변수 rows 선언		
		int rows=0;
		
		///// 검색 오류 체크에 따른 게시물 존재 여부 확인 /////
		if(search==null){
		//처음 들어왔을 경우
			//게시판에 게시물이 있는지 유무 판단 위한 sql
			ResultSet rs = stmt.executeQuery("select count(*) from gongji2;");
			// 총 게시글 수
			while (rs.next()){
				rows = rs.getInt(1);
			}
			rs.close();
			stmt.close();
		}else if(search.isEmpty()){
		//빈공백 검색했을 경우
			%>	<!-- 공백검색할 경우 alert창 띄우며 알리고 list.jsp로 이동-->
				<script type="text/javascript">
				alert("검색결과가 없습니다.");
				window.location.href="gongji_list.jsp";
				</script>
			<%
		}else{
		//검색할 경우
			// 검색결과가 있는지 확인하는 sql문
			ResultSet rs = stmt.executeQuery("select count(*) from gongji2 where "+opt+"='"+search+"';");
			rs.next();
			rows = rs.getInt(1);
			// 검색결과가 없을 경우 if문 실행
			if(rows==0){
				%>	<!--없을 경우 alert창 띄우며 알리고 list.jsp로 이동-->
				<script type="text/javascript">
				alert("검색결과가 없습니다.");
				window.location.href="gongji_list.jsp";
				</script>
				<%
			}
			rs.close();
			stmt.close();
		}
		//////
		
		///// 페이징을 위한 변수 선언과 조건들 /////
		//페이지당 게시물 출력 수
		int cntPT=10;
		// 총페이지 수
		int pages=rows/cntPT;
		if((rows%cntPT)!=0){
			pages++;
		}
		//현재페이지
		int npage=0;
		try{
			String npages = request.getParameter("page");
			npage=Integer.parseInt(npages);
		}catch(Exception e){
		// gongji_list_jsp에 처음들어왔을 때 null일때의 경우
			npage=1;
		}
		
		// fromPT 페이지당 게시물 시작 번호
		int fromPT = (npage-1)*cntPT;
		
		block=5;
		
		int spage=0;
		spage=(npage-1)/block*block+1;
		
		
		int epage=0;
		epage=spage+block;
		if(epage>pages) {
			epage=pages+1;
		}
		
		//이전페이지 버튼 설정
		int bepage=spage-block;
		if(bepage<1){
			bepage=1;
		}
		//다음페이지 버튼 설정
		int afpage=spage+block;
		if(afpage>pages){
			afpage=pages;
		}
		
		//out.println("spage:"+spage);
		//out.println("epage:"+epage);
		//out.println("npage:"+npage);
		//out.println("fromPT:"+fromPT);
		//out.println("pages:"+pages);
		//out.println("rows:"+rows);
		//out.println("cntPT(=limit):"+cntPT);
		/////
		
		if(search==null){
		//처음 들어왔을 경우
			//fromPT부터 cntPT수만큼 출력하기 위한 sql
			sql="select * from gongji2 order by rootid desc, recnt asc limit "+fromPT+","+cntPT+";";
			
		}else{
		//검색할 경우
			// 검색 결과를 출력하기 위한 sql문
			sql="select * from gongji2 where "+opt+"='"+search+"' order by rootid desc, recnt asc limit "+fromPT+","+cntPT+";";
					
		}
		
		//게시판 리스트 출력 sql
		ResultSet rset = stmt2.executeQuery(sql);
				
		%>
		<table id="top" cellspacing="0">
		<tr>
			<td><h1>게시글 목록</h1></td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td id="end">현재 페이지 : <b><%=npage%></b> 페이지 </td>
		</tr>
		</table>
		<table cellspacing="0" width="1000" border="1">
		<tr>
			<th width="10%"> 번호 </th>
			<th width="70%"> 제목 </th>
			<th width="10%"> 조회수 </th>
			<th width="10%"> 등록일 </th>
		<!-- 댓글 우선순위 체크용
		<th width="10%" align="center"> rootid </th>
		<th width="10%" align="center"> recnt </th>
		<th width="10%" align="center"> relevel </th>
		-->
		
		</tr>
		<%	//게시판에 게시물이 있을 경우 if문 실행
		if(rows!=0){ 
			while (rset.next()) {
				%>
					<tr>
						<td><%=rset.getInt(1)%></td>
						<td id="title">
							<%	//댓글이 있는지 유무 판단하기 위한 if문
							if(rset.getInt(6)>0){
								//댓글이 댓글인지 대댓글인지 판단아여 들여쓰기 판정
								for(int i=0; i<rset.getInt(6); i++) {
								%>&emsp;&emsp;<%					
								}
								%>	<!--댓글을 의미하는 화살표 이미지-->
								<img src="arrow.png" width="20" heigh="20">
								<%
							}
							%>
							<a href="gongji_view.jsp?id=<%=rset.getInt(1)%>"><%=rset.getString(2)%>
							<%	//게시물의 작성날짜와 오늘 날짜가 같은지 판단
								if(today.equals(rset.getString(3))){
								%> <!--// 같을 경우 new라는 이미지 출력-->
								<img src="new.png" width="20" heigh="20">
								<%
								}
							%>
						</td>
						<td><%=rset.getInt(8)%></td>
						<td><%=rset.getDate(3)%></td>
						<!-- 댓글 우선순위 체크용
						<td align=center><%=rset.getInt(5)%></td>
						<td align=center><%=rset.getInt(7)%></td>
						<td align=center><%=rset.getInt(6)%></td>
						-->
					</tr>
				<%
			}
			%>
			</table>

		<%	// 게시판에 게시물이 없을 경우 else문 실행	
		}else{
			%>
			<tr>
				<td colspan="4"><p align="center">게시글이 없음</p></td>
			</tr>
		<%
		}
		%>
		</table>
		<table cellspacing="0">
		<tr id="tail">
		<td colspan="3">
		<%
		// 검색 아닐때만 방향표가 출력
		if(search==null){
			%>
			<a id="page" href="gongji_list.jsp?page=1">&lt;&lt;</a>
			<a id="page" href="gongji_list.jsp?page=<%=bepage%>">&lt;</a>
			<%
		}
		for(int i=spage; i<epage; i++){
			if(npage==(i)){
			%> <!--현재 페이지일 경우 현재페이지 태그 칸 배경색 표시 -->
				<a id="page" style="background-color:CadetBlue; color:white;" href="gongji_list.jsp?page=<%=(i)%>"><%=(i)%></a>
			<%
			}else{
			%>	<!--그 외의 경우-->
				<a id="page" href="gongji_list.jsp?page=<%=(i)%>"><%=(i)%></a>
			<%
			}
		}
		// 검색 아닐때만 방향표가 출력
		if(search==null){
		%>
			<a id="page" href="gongji_list.jsp?page=<%=afpage%>">&gt;</a>
			<a id="page" href="gongji_list.jsp?page=<%=pages%>">&gt;&gt;</a>
		<%
		}
		%>
		</td>
		</tr>
		<tr id="tail">
			<td width="20%"></td>
			<td width="60%">
			<form method="post" action="gongji_list.jsp">
			<select name="opt">	<!--opt라는 변수명으로 value값을 전송-->
				<option value="id">번호</option>
				<option value="title">제목</option>
			</select>
			<!--search라는 변수명으로 입력받은 값을 전송-->
			<input type="search" name="search" placeholder="Search.." maxlength="100"><input type="submit" value="검색">
			</form>
			</td>
			<td id="end" width="20%">
			<!--목록이라는 버튼을 클릭시 list.jsp로 이동-->
			<input type="button" value="목록" onclick="window.location.href='gongji_list.jsp'">
			<!--신규라는 버튼을 클릭시 insert.jsp로 이동-->
			<input type="button" value="신규" onclick="window.location.href='gongji_insert.jsp'">
			</td>
		</tr>
		<%
		// 닫기
		rset.close();
		stmt2.close();
		conn.close();
		
	// mysql 데이터베이스 실패시
	}catch(Exception e){
		%>
		<script type="text/javascript">
		alert("DB연결에 실패했습니다. 잠시 후에 다시 접속해주세요.");
		</script>
		<%
	}
	%>
</center>
</body>
</html>