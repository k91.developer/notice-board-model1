<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*,javax.sql.*,java.io.*,java.util.*,java.text.*" %>
<html>
<head>
<title>KSW01_Board</title>
<!--mystyle.css를 적용하기 위해 import-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<style>
#comment {
	width: 96%;
}
</style>
<!--전역변수 선언-->
<%!int rootid;%>
<%!int relevel;%>

<%	// 현재 시간을 알아내기 위한
	TimeZone tz;
	java.util.Date date = new java.util.Date();
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	tz = TimeZone.getTimeZone("Asia/Seoul"); df.setTimeZone(tz);
	// 한글깨짐 방지
	request.setCharacterEncoding("UTF-8");
	// 해당 id 조회하기 위한 파라미터
	int id;
	String ids = request.getParameter("id");
	id=Integer.parseInt(ids);
%>
<script>
<!--특수문자 입력을 막기 위한 함수-->
function checkNumber()
{
	var objEv = event.srcElement;
	var num ="{}[]()<>?_|~`!@#$%^&*-+\"'\\/";    //입력을 막을 특수문자 기재.
	event.returnValue = true;
  
	for (var i=0;i<objEv.value.length;i++){
		if(-1 != num.indexOf(objEv.value.charAt(i)))
		event.returnValue = false;
	}
	if (!event.returnValue){
		alert("특수문자는 입력하실 수 없습니다.");
		var a="";
		for (var i=0;i<objEv.value.length-1;i++){
			a+=objEv.value.charAt(i);
		}
		objEv.value=a;
	}
}
</script>
</head>
<body>
<center>
<%	// 객체 생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB연결
	Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/kopoctc","root","1234");
	// 해당 id의 데이터를 조회하기 위한 sql문
	Statement stmt=conn.createStatement();
	ResultSet rs=stmt.executeQuery("select * from gongji2 where id = " + id + ";");
	int recnt=0;
	while(rs.next()){
		rootid=rs.getInt(5);
		relevel=rs.getInt(6);
		recnt=rs.getInt(7);
		recnt++;
		relevel++;
	}
	// 닫기
	rs.close();
	stmt.close();
	// 대댓글의 경우 cnt 빈곳 생성하기 위한 SQL문
	Statement stmt3=conn.createStatement();
	stmt3.execute("update gongji2 set recnt=recnt+1 where rootid="+rootid+" and recnt >= "+recnt+";");
	stmt3.close();
%>
	<table id="top" cellspacing="0">
		<tr>
			<td><h1>댓글 작성</h1></td>
		</tr>
	</table>
	<form method="post" action="gongji_write.jsp" enctype="multipart/form-data">
	<table border="1" width="1000" cellspacing="0">
		<tr>
			<th width="20%" align="center">번호</th>
			<td colspan="3" width="80%" >댓글 <input id="comment" type="text" maxlength="5" name="id" value="INSERT"></td>
		</tr>
		<tr>
			<th align="center">제목</th>
			<td colspan="3"><input type="text" name="title" onKeyDown="checkNumber();" pattern="^[\S.+]+" required maxlength="70" placeholder="70자 이내로 작성하시오.(특수문자 제외)"></td>
		</tr>
		<tr>
			<th align="center">일자</th>
			<td colspan="3"><%=df.format(date)%></td>
		</tr>
		<tr>
			<th align="center">내용</th>
			<td colspan="3"><textarea name="content" onKeyDown="checkNumber();" required maxlength="1000"></textarea></td>	
		</tr>
		<tr>
			<th width align="center">원글</th>
			<td width colspan="3"><input type="number" name="rootid" value="<%=rootid%>" readonly></td>
		</tr>
		<tr>
			<th width="20%" align="center">댓글수준</th>
			<td width="30%" ><input type="number" name="relevel" value="<%=relevel%>" readonly></td>
			<th width="20%" align="center">댓글내 순서</th>
			<td width="30%" ><input type="number" name="recnt" value="<%=recnt%>" readonly></td>
		</tr>
		
	</table>
	<table cellspacing="0">
		<tr>
			<td><input type="submit" value="쓰기">
	</form>	
				<input type="button" class="button" value="취소" onclick="location.href='gongji_list.jsp'">
			</td>
		</tr>
	</table>
<%	// 닫기
	conn.close();
%>
</center>
</body>
</html>