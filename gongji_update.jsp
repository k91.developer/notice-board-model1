<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<head>
<title>KSW01_Board</title>

<!-- include libraries(jQuery, bootstrap) -->
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 
<!-- include summernote css/js-->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>

<!--mystyle.css를 적용하기 위해 import-->
<link rel="stylesheet" type="text/css" href="mystyle.css">

<!-- 전역변수 선언 -->
<%!int rootid;%>
<%!int recnt;%>
<%!String file;%>

<!-- summernote에 대한 sytle-->
<script type="text/javascript">
    $(function() {
      $('#summernote').summernote({
        tabsize: 2,
		height: 500,	
      });  
    });
  </script>

<%	// 한글깨짐 방지
	request.setCharacterEncoding("UTF-8");
	// 해당 id를 조회하기 위한 파라미터
	int id;
	String ids = request.getParameter("id");
	id=Integer.parseInt(ids);	
%>

<script>
<!--특수문자 입력을 막기 위한 함수-->
function checkNumber()
{
	var objEv = event.srcElement;
	var num ="{}[]()<>?_|~`!@#$%^&*-+\"'\\/";    //입력을 막을 특수문자 기재.
	event.returnValue = true;
  
	for (var i=0;i<objEv.value.length;i++){
		if(-1 != num.indexOf(objEv.value.charAt(i)))
		event.returnValue = false;
	}
	if (!event.returnValue){
		alert("특수문자는 입력하실 수 없습니다.");
		var a="";
		for (var i=0;i<objEv.value.length-1;i++){
			a+=objEv.value.charAt(i);
		}
		objEv.value=a;
	}
}
</script>
</head>
<body>
<center>
<%	//객체생성
	Class.forName("com.mysql.jdbc.Driver");
	
	//DB연결
	Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/kopoctc","root","1234");
	// 해당 id의 데이터를 조회하기 위한
	Statement stmt=conn.createStatement();
	ResultSet rs=stmt.executeQuery("select * from gongji2 where id = " + id + ";");
	%>

	<table id="top" cellspacing="0">
			<tr>
				<td><h1 style="font-weight: 700;">게시글 수정</h1></td>
			</tr>
	</table>
	<table cellspacing="0" border="1">
	<form method="post" action="gongji_write.jsp" enctype="multipart/form-data">
		<%
		while(rs.next()){
			rootid=rs.getInt(5);
			recnt=rs.getInt(7);
			file=rs.getString(9);
			//out.println("recnt:"+recnt);
			//out.println("rootid:"+rootid);
			%>
			<tr>
				<th width="20%">번호</th>
				<td width="80%"><input type="number" readonly name="id" value="<%=rs.getInt(1)%>"></td>
			</tr><tr>
				<th>제목</th>
				<td><input type="text" size="35" onKeyDown="checkNumber();" pattern="^[\S]+" required name="title" value="<%=rs.getString(2)%>"></td>
			</tr><tr>
				<th>일자</th>
				<td><%=rs.getDate(3)%></td>
			<!--
			</tr><tr>
				<th>내용</th>	
				<td><textarea cols="40" rows="8" onKeyDown="checkNumber();" required name=content><%=rs.getString(4)%></textarea></td>
			-->
			</tr><tr>
				<th>내용</th>
				<td><textarea cols="40" rows="8" id="summernote" name="content"><%=rs.getString(4)%></textarea></td>
			</tr>
			<%
		}
	%>
	</table>
	<table cellspacing="0">
	<td>
	<input type="button" value="취소" onclick="window.location.href='gongji_view.jsp?id=<%=id%>'">
	<input type="submit" value="수정">
	</form>
	<input type="button" value="삭제" onclick="window.location.href='gongji_delete.jsp?id=<%=id%>&rootid=<%=rootid%>&recnt=<%=recnt%>&file=<%=file%>'">
	</td>
	</table>
	<%
		// 닫기
		rs.close();
		stmt.close();
		conn.close();
	%>
</center>
</body>
</html>