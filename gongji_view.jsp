<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<head>
<title>KSW01_Board</title>
<!--전역변수 id, viewer 선언-->
<%! int id; %>
<%! int viewer; %>
<%! String file; %>
<%	// 한글 깨짐 방지
	request.setCharacterEncoding("UTF-8");
	// 해당 id 조회를 위한 파라미터
	String ids = request.getParameter("id");
	id=Integer.parseInt(ids);
%>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
</head>
<body>
<center>
<%	// 객체생성
	Class.forName("com.mysql.jdbc.Driver");
	try{
		// DB연결
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/kopoctc","root","1234");
		// stmt 해당 id의 데이터를 조회하기 위한
		Statement stmt=conn.createStatement();
		ResultSet rs=stmt.executeQuery("select * from gongji2 where id = " + id + ";");
		%>
		<table id="top" cellspacing="0">
				<tr>
				<td rowspan=2><h1>게시글 보기</h1></td>
				</tr>
		</table>
		<table cellspacing="0" border="1">
		<%
			while(rs.next()){
				// viewer - 조회수
				viewer=rs.getInt(8);
				file=rs.getString(9);
				%>
				<tr>
					<th width="20%">번호</th>
					<td class="view" colspan="3" width="90%"><%=rs.getInt(1)%></td>
				</tr><tr>
					<th>제목</th>
					<td class="view" colspan="3"><%=rs.getString(2)%></td>
				</tr><tr>
					<th>일자</th>
					<td class="view" colspan="3"><%=rs.getDate(3)%></td>
				</tr><tr>
					<th>조회수</th>
					<td class="view" colspan="3"><%=rs.getInt(8)%></td>
				</tr><tr>
					<th>내용</th>
					<td class="view" colspan="3"><%=rs.getString(4)%></td>
				</tr><tr>
					<th>첨부파일</th>
					<%
					if(file==null){
					%>	<!--파일이 없을 경우-->
						<td colspan="3" class="view">등록된 파일이 없습니다.</td>
					<%	
					}else{
					%>	<!--파일이 있을 경우-->
						<td colspan="3"><input type="text" value="<%=rs.getString(9)%>" disabled></td>
					<%	
					}
					%>
				</tr><tr>
					<th width="20%">원글</th>
					<td width="30%" colspan="3"><%=rs.getInt(5)%></td>
				</tr><tr>
					<th width="20%">댓글수준</th>
					<td width="30%"><%=rs.getInt(6)%></td>
					<th width="20%">댓글내 순서</th>
					<td width="30%"><%=rs.getInt(7)%></td>
				</tr><tr>
				<%
				// 조회할 때마다 조회수 1씩 증가
				viewer++;
			}
		// stmt2 해당 id조회할때마 viewer수를 1씩 증가시키기 위한	
		Statement stmt2=conn.createStatement();
		stmt2.execute("update gongji2 set viewcnt="+viewer+" where id = " + id + ";");
		// 닫기
		stmt2.close();
		rs.close();
		stmt.close();
		conn.close();
		%>
		</table>
		<table cellspacing="0">
			<td id="end">
			<input type="button" value="목록" onclick="window.location.href='gongji_list.jsp'">
			<input type="button" value="수정" onclick="window.location.href='gongji_update.jsp?id=<%=id%>'">	
			<input type="button" value="댓글" onclick="window.location.href='gongji_test.jsp?id=<%=id%>'">	
			</td>
		</table>
	<%
	// mysql 데이터베이스 실패시
	}catch(Exception e){
		%>
		<script type="text/javascript">
		alert("DB연결에 실패했습니다. 잠시 후에 다시 접속해주세요.");
		</script>
		<%
	}
	%>
</center>
</body>
</html>