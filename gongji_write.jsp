<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<%@ page import="java.util.*, java.text.*"  %>
<html>
<head>
<title>KSW01_Board</title>
<!--전역변수 선언-->
<%! int relevel;%>
<%! int recnt;%>
<%! int rootid;%>
<%	
	// 현재 시간을 알아내기 위한
	TimeZone tz;
	java.util.Date date = new java.util.Date();
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	tz = TimeZone.getTimeZone("Asia/Seoul"); df.setTimeZone(tz);
%>
</head>
<body>
<%	
	// 실제로 서버에 저장되는 path
   	String path = request.getRealPath("/File");
    // 파일 사이즈 설정 : 10M
	int size = 5 * 1024 * 1024; 
	// DefaultFileRenamePolicy 처리는 중복된 이름이 존재할 경우 처리할 때
    // request, 파일저장경로, 용량, 인코딩타입, 중복파일명에 대한 정책
	
	MultipartRequest multi = new MultipartRequest(request, path, size, "UTF-8", new DefaultFileRenamePolicy());
	// 넘어오는 값을 받기 위한 파라미터들(공통)
	
	// 한글깨짐 방지
	request.setCharacterEncoding("UTF-8");
	// 해당 id 조회하기 위한 파라미터
	String id = multi.getParameter("id");
	String title = multi.getParameter("title");
	String content = multi.getParameter("content");
	
	
	// 객체생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB연결
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	// 데이터 추가일 경우
	if(id.equals("INSERT")){
		try{
			// 댓글일 경우
			String relevels=multi.getParameter("relevel");
			relevel=Integer.parseInt(relevels);
			
		}catch(Exception e){
			// 댓글이 아닌 게시글일 경우
			relevel=0;
		}
		
		try{
			// 댓글일 경우
			String recnts=multi.getParameter("recnt");
			recnt=Integer.parseInt(recnts);
			
		}catch(Exception e){
			// 댓글이 아닌 게시글일 경우
			recnt=0;
		}
		
		try{
			// 댓글일 경우
			String rootids=multi.getParameter("rootid");
			rootid=Integer.parseInt(rootids);
		}catch(Exception e){
			// 댓글이 아닌 게시글일 경우
			// stmt2 gongji2의 id 최댓값 구하기 위한
			Statement stmt2 = conn.createStatement();
			ResultSet rs = stmt2.executeQuery("select max(id) from gongji2;");
			while(rs.next()){
				rootid=rs.getInt(1)+1;
			}
			rs.close();
			stmt2.close();
		}
		
		String fileName = "";    // 업로드한 파일 이름
		String originalFileName = "";    //  서버에 중복된 파일 이름이 존재할 경우 처리하기 위해
		// cos.jar라이브러리 클래스를 가지고 실제 파일을 업로드하는 과정
					
		// 전송한 전체 파일이름들을 가져온다.
		Enumeration files = multi.getFileNames();
		String str="";
		try{
			str = (String)files.nextElement();
		}catch(Exception e){
			str="";
		}
			
		//파일명 중복이 발생했을 때 정책에 의해 뒤에 1,2,3 처럼 숫자가 붙어 고유 파일명을 생성한다.
		// 이때 생성된 이름을 FilesystemName이라고 하여 그 이름 정보를 가져온다. (중복 처리)
		fileName = multi.getFilesystemName(str);
				
		if(fileName==null){
		// 파일이 첨부되지 않았을 경우
			// 새로운 글을 빈 우선순위에 데이터를 추가하기 위한 sql문
			Statement stmt = conn.createStatement();
			stmt.execute("insert into gongji2 (title,date,content,rootid,relevel,recnt) values ('"+title+"','"+df.format(date)+"','"+content+"',"+rootid+","+relevel+","+recnt+");");
			//out.println("결과 : "+title+" 게시글이 추가되었습니다.");
			// 닫기
			stmt.close();
			%> <!--등록 완료 알림-->
			<script type="text/javascript">
			alert("게시글이 등록되었습니다.");
			window.location.href="gongji_list.jsp";
			</script>
			<%
		}else{
		//파일이 첨부되었을 경우	
			originalFileName = multi.getOriginalFileName(str);
			out.println("절대 경로 : " + path + "/"+fileName+"<br/>");
			String rpath;
			rpath="/File/"+fileName;	
			// 새로운 글을 빈 우선순위에 데이터를 추가하기 위한 sql문
			Statement stmt = conn.createStatement();
			stmt.execute("insert into gongji2 (title,date,content,rootid,relevel,recnt,path) values ('"+title+"','"+df.format(date)+"','"+content+"',"+rootid+","+relevel+","+recnt+",'"+rpath+"');");
			//out.println("결과 : "+title+" 게시글이 추가되었습니다.");
			// 닫기
			stmt.close();
			%> <!--등록 완료 알림-->
			<script type="text/javascript">
			alert("게시글이 등록되었습니다.");
			window.location.href="gongji_list.jsp";
			</script>
			<%	
		}
	}else{
		// 수정일 경우
		// stmt2 해당 id가 존재하는지 파악하기 위한 sql문
		Statement stmt2 = conn.createStatement();
		ResultSet rs=stmt2.executeQuery("select id from gongji2 where id="+id+";");
		int result=0;
		while(rs.next()){
			result=rs.getInt(1);
		}
		if(result==0){
		// 수정할 id가 없을 경우
			%> <!--잘못된 번호 알림-->
			<script type="text/javascript">
			alert("잘못된 번호입니다.");
			history.back();
			</script>
			<%
		}else{
		// 수정할 id가 존재할 경우
			// stmt3 해당 id의 데이터를 가져오기 위한 sql문
			Statement stmt3 = conn.createStatement();
			ResultSet rset=stmt3.executeQuery("select * from gongji2 where id="+id+"");
			rset.next();
			int relevel =rset.getInt(6);
			int recnt=rset.getInt(7);
			// stmt 해당 id의 데이터를 수정하기 위한 sql문
			Statement stmt = conn.createStatement();
			stmt.execute("update gongji2 set title='"+title+"', date='"+df.format(date)+"' ,content='"+content+"', relevel="+relevel+", recnt="+recnt+" where id="+id+";");
			//out.println("결과 : "+title+" 게시글이 수정되었습니다.");
			// 닫기
			stmt.close();
			rset.close();
			stmt3.close();
			
			%> <!--수정 완료 알림-->
			<script type="text/javascript">
			alert("게시글이 수정되었습니다.");
			window.location.href="gongji_list.jsp";
			</script>
			<%
		}
		// 닫기
		rs.close();
		stmt2.close();
	}
	//닫기
	conn.close();
%>
</body>
</html>